export default async function addFont (document: any) {
  if (document.fonts.check('10px Montserrat')) return

  const fontFace = new FontFace('Montserrat', `url('https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;900&display=swap')`)

  return fontFace.load()
    .then(font => document.fonts.add(font))
    .catch(err => console.log('font load failed', err))
}
