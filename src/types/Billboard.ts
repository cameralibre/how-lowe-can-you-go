import type { ColorInterpolationProperty } from "csstype";
import type ColorPair from '@/types/ColorPair'

export default interface Billboard {
  id: string;
  path: string;
  thumb: string;
  width: number;
  height: number;
  origin: [number, number]; // currently values between 0 & 1, indicating how far across/down the target shape the text origin is.
  alignVertical: ('top' | 'center');
  alignHorizontal: ('left' | 'center');
  targetAspectRatio: number;
  targetShape: TargetShape;
  text: string;
  fontWeight: number;
  fontSize: number;
  lineSpacing: number;
  colors: ColorPair;
}

interface TargetShape {
  topLeftX: number;
  topLeftY: number;
  topRightX: number;
  topRightY: number;
  bottomRightX: number;
  bottomRightY: number;
  bottomLeftX: number;
  bottomLeftY: number;
}
