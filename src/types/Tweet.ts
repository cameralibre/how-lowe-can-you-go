export default interface Tweet {
  id: string;
  avatar: string;
  displayName: string;
  userName: string;
  text: string;
  url: string;
  hashtag: string;
  mention: string;
  image: string;
  viewBox: string; // SVG Viewbox, for centering/cropping the image
  time: string; // should eventually be Date
  likes: number;
  retweets: number;
}