export default interface RedditPost {
  id: string;
  subreddit: string;
  userName: string;
  avatar: string;
  postUrl: string;
  text: string;
  image: string;
  viewBox: string; // SVG Viewbox, for centering/cropping the image
  time: string; // should eventually be Date
  upvotes: number;
  comments: number;
}