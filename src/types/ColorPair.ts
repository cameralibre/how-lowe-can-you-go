export default interface ColorPair {
  white: `hsla(${number}, ${number}%, ${number}%, ${number})`;
  red: `hsla(${number}, ${number}%, ${number}%, ${number})`;
}