export default interface TextStyle {
  text: string,
  fillStyle: string
}
