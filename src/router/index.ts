import { createRouter, createWebHistory } from 'vue-router'
import BillboardIndex from '../views/BillboardIndex.vue'
import BillboardShow from '../views/BillboardShow.vue'
// import SellingView from '../views/SellingView.vue'
import AboutView from '../views/AboutView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: BillboardIndex
    },
    {
      path: '/billboard/:id',
      name: 'billboardShow',
      component: BillboardShow
    },

    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // {
    //   path: '/selling',
    //   name: 'selling',
    //   component: SellingView
    // },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    }
  ]
})

export default router
