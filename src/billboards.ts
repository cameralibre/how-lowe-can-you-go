import type Billboard from './types/Billboard'

export const weLoveNewtown: Billboard = {
  id: 'we-love-newtown',
  path: new URL('./assets/billboards/we-love-newtown.jpg', import.meta.url).href,
  thumb: new URL('./assets/thumbnails/we-love-newtown_thumb.jpg', import.meta.url).href,
  width: 2000,
  height: 1500,
  origin: [0.3, 0.13],
  alignVertical: 'top',
  alignHorizontal: 'left',
  targetAspectRatio: 1.8428 * 1.1, // script output * fudge factor
  targetShape: {
    topLeftX: 675,
    topLeftY: 327,
    topRightX: 1709,
    topRightY: 94,
    bottomRightX: 1709,
    bottomRightY: 777,
    bottomLeftX: 574,
    bottomLeftY: 839
  },
  text: 'We *love*\nNewtown.',
  fontWeight: 900,
  fontSize: 300,
  lineSpacing: 1,
  colors: {
    white: 'hsla(14, 13.6%, 90.8%, 0.75)',
    red: 'hsla(354, 65.6%, 61%, 0.75)'
  }
}

export const toReachTheTop: Billboard = {
  id: 'to-reach-the-top',
  path: new URL('./assets/billboards/to-reach-the-top.jpg', import.meta.url).href,
  thumb: new URL('./assets/thumbnails/to-reach-the-top_thumb.jpg', import.meta.url).href,
  width: 1500,
  height: 2000,
  origin: [0.08, 0.08],
  alignVertical: 'top',
  alignHorizontal: 'left',
  targetAspectRatio: 0.6014 * 0.9, // script output * fudge factor
  targetShape: {
    topLeftX: 699,
    topLeftY: 708,
    topRightX: 1077,
    topRightY: 807,
    bottomRightX: 1087,
    bottomRightY: 1531,
    bottomLeftX: 678,
    bottomLeftY: 1477
  },
  text: 'To reach\nthe top,\nstart\nat the\nbottom\nright-hand\ncorner.',
  fontWeight: 600,
  fontSize: 135,
  lineSpacing: 1.2,
  colors: { 
    white: 'hsla(230, 13.7%, 92%, 0.75)',
    red: 'hsla(354, 52.7%, 52%, 0.75)'
  }
}

export const anotherGreatWay: Billboard = {
  id: 'another-great-way',
  path: new URL('./assets/billboards/another-great-way.jpg', import.meta.url).href,
  thumb: new URL('./assets/thumbnails/another-great-way_thumb.jpg', import.meta.url).href,
  width: 1500,
  height: 2000,
  origin: [0.13, 0.07],
  alignVertical: 'top',
  alignHorizontal: 'left',
  targetAspectRatio: 1.14, // script output * fudge factor
  targetShape: {
    topLeftX: 461,
    topLeftY: 362,
    topRightX: 1108,
    topRightY: 464,
    bottomRightX: 1172,
    bottomRightY: 1043,
    bottomLeftX: 499,
    bottomLeftY: 1074
  },
  text: 'Another great way\nto move suburbs.',
  fontWeight: 600,
  fontSize: 180,
  lineSpacing: 1.2,
  colors: {
    white: 'hsla(208, 30%, 83%, 0.75)',
    red: 'hsla(356, 85%, 52%, 0.4)'
  }
}

export const perfectlyAppointed: Billboard = {
  id: 'perfectly-appointed',
  path: new URL('./assets/billboards/perfectly-appointed.jpg', import.meta.url).href,
  thumb: new URL('./assets/thumbnails/perfectly-appointed_thumb.jpg', import.meta.url).href,
  width: 2000,
  height: 1500,
  origin: [0.04, 0.16],
  alignVertical: 'top',
  alignHorizontal: 'left',
  targetAspectRatio: 2.4, 
  targetShape: {
    topLeftX: 471,
    topLeftY: 464,
    topRightX: 1761,
    topRightY: 19,
    bottomRightX: 1873,
    bottomRightY: 1058,
    bottomLeftX: 400,
    bottomLeftY: 1056
  },
  text: 'Perfectly\nappointed.',
  fontWeight: 600,
  fontSize: 230,
  lineSpacing: 1.2,
  colors: {
    white: 'hsla(213, 44.8%, 82%, 0.75)',
    red: 'hsla(348, 44%, 48%, 0.75)'
  }
}

export const weCouldTalkAbout: Billboard = {
  id: 'we-could-talk-about',
  path: new URL('./assets/billboards/we-could-talk-about.jpg', import.meta.url).href,
  thumb: new URL('./assets/thumbnails/we-could-talk-about_thumb.jpg', import.meta.url).href,
  width: 2000,
  height: 1500,
  origin: [0.06, 0.16],
  alignVertical: 'top',
  alignHorizontal: 'left',
  targetAspectRatio: 2, 
  targetShape: {
    topLeftX: 584,
    topLeftY: 254,
    topRightX: 1796,
    topRightY: 372,
    bottomRightX: 1810,
    bottomRightY: 965,
    bottomLeftX: 570,
    bottomLeftY: 895
  },
  text: 'We could talk about being\nthe best in Wellington.\nBut we\'d rather show you.',
  fontWeight: 600,
  fontSize: 184,
  lineSpacing: 1.2,
  colors: {
    white: 'hsla(230, 11%, 89%, 0.75)',
    red: 'hsla(355, 62%, 50%, 0.75)'
  }
}

export const weCanSeeYourHouse: Billboard = {
  id: 'we-can-see-your-house',
  path: new URL('./assets/billboards/we-can-see-your-house.jpg', import.meta.url).href,
  thumb: new URL('./assets/thumbnails/we-can-see-your-house_thumb.jpg', import.meta.url).href,
  width: 1500,
  height: 2000,
  origin: [0.04, 0.18],
  alignVertical: 'top',
  alignHorizontal: 'left',
  targetAspectRatio: 3.8, 
  targetShape: {
    topLeftX: 96,
    topLeftY: 474,
    topRightX: 1186,
    topRightY: 437,
    bottomRightX: 1196,
    bottomRightY: 699,
    bottomLeftX: 69,
    bottomLeftY: 733
  },
  text: 'We can see your next\nhouse from here.',
  fontWeight: 600,
  fontSize: 455,
  lineSpacing: 1.2,
  colors: {
    white: 'hsla(207, 24%, 70%, 0.75)',
    red: 'hsla(343, 79%, 34%, 0.75)'
  }
}

export default [
  weCanSeeYourHouse,
  weCouldTalkAbout,
  weLoveNewtown,
  toReachTheTop,
  anotherGreatWay,
  perfectlyAppointed
]
