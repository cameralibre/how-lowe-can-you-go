# How Lowe Can You Go
The [How Lowe Can You Go](loweand.co.nz) billboard editing tool.

![_'How Lowe Can You Go' in use_](how-lowe-can-you-go.mp4)

## Technologies used: 
- [Vue 3](vuejs.org/)
- [Typescript](typescriptlang.org/)
- [Perspective.ts](https://github.com/adonmo/perspective.ts) (fork of [perspective.js](https://github.com/wanadev/perspective.js)) - stretching text elements to fit the perspective and shape of the target billboard
- [Web Share API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Share_API) - using the native sharing function of the operating system (where supported)
- [Clipboard API](https://developer.mozilla.org/en-US/docs/Web/API/Clipboard_API) - copying an image or link to the clipboard


## License
The project is under [🚩 The Anti-Capitalist Software License](https://anticapitalist.software/), so this code may be re-used and adapted by individuals, non-profits, educational institutions, and worker-owned cooperatives.

The license forbids use by capitalist organisations, military and law enforcement.

## Technical setup
This template should help get you started developing with Vue 3 in Vite.

### Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

### Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

### Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

### Project Setup

```sh
npm install
```

#### Compile and Hot-Reload for Development

```sh
npm run dev
```

#### Type-Check, Compile and Minify for Production

```sh
npm run build
```

#### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
